employee_dict = {}

while True:
    empid = input("Enter employee ID (or 'exit' to stop): ")
    if empid.lower() == 'exit':
        break
    empname = input("Enter employee name: ")
    years_exp = int(input("Enter years of experience: "))

    # add employee to dictionary
    employee_dict[empid] = {'empname': empname, 'years_exp': years_exp}

print(employee_dict)