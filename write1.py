import openpyxl

# Create a new Excel workbook
workbook = openpyxl.Workbook()

# Select the worksheet you want to write to
worksheet = workbook.active

# Write data to the worksheet
worksheet['A1'] = 'Employee ID'
worksheet['B1'] = 'Employee Name'
worksheet['C1'] = 'Years of Experience'

data = [
    ('1001', 'John Smith', 5),
    ('1002', 'Jane Doe', 10),
    ('1003', 'Bob Johnson', 2)
]

for row in data:
    worksheet.append(row)

# Save the workbook to a file
workbook.save('example.xlsx')