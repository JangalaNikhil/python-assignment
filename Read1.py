import openpyxl

# Load the Excel workbook
workbook = openpyxl.load_workbook('example.xlsx')

# Select the worksheet you want to read from
worksheet = workbook['Sheet1']

# Iterate through the rows and columns of the worksheet
for row in worksheet.iter_rows(min_row=2, values_only=True):  # Start from row 2 to skip header
    empid, empname, years_exp = row
    print(f"Employee ID: {empid}, Name: {empname}, Years of Experience: {years_exp}")